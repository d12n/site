from datetime import datetime
from pathlib import Path
from urllib.parse import urljoin

import frontmatter
import tomli
from pytz import timezone
from icalendar import Calendar, Event
from pypandoc import convert_text
from slugify import slugify
from staticjinja import Site

import builders


def now():
    tz = timezone("Europe/Amsterdam")
    return datetime.now(tz=tz).isoformat(timespec="minutes", sep=" ")


def dtfmt(dt, time=True):
    if time:
        return dt.strftime("%-d %B %Y • %H:%M (%Z)")
    else:
        return dt.strftime("%-d %B %Y")


def markdown(doc):
    return convert_text(
        doc, "html", format="md", extra_args=["--shift-heading-level-by=1"]
    )


def entries(site, template, **kwargs):
    for sub in Path("content").iterdir():
        if sub.is_dir() and not sub.name.startswith("_"):
            cat = sub.name
            if hasattr(builders, cat):
                getattr(builders, cat)(sub, site, template, **kwargs)
            else:
                builders.default(sub, site, template, **kwargs)


if __name__ == "__main__":
    with open("config.toml", "rb") as f:
        config = tomli.load(f)
    site = Site.make_site(
        rules=[("page.html", entries)],
        filters=[("markdown", markdown), ("dt", dtfmt)],
        outpath="public",
        env_globals={"now": now(), **config},
    )
    site.render()
