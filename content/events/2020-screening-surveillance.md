---
title: Screening Surveillance Film Screening
start: 2023-03-20 11:00:00+1
end: 2023-03-20 13:30:00+1
location: online (Kaltura Live Room)
---

# Screening Surveillance

## A film screening featuring producer sava saheli singh

[More information][ul_link]

[ul_link]: https://www.universiteitleiden.nl/en/events/2020/12/screening-surveillance
