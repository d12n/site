---
title: Digitization, Scholarly Publishing & Knowledge Production
start: 2019-12-11 15:00:00+1
end: 2019-12-11 16:30:00+1
location: PdlC 1A25
---

# Digitization, Scholarly Publishing & Knowledge Production

## A conversation with Sean Jacobs of Africa is a Country

Also featuring Africa is a Country contributor Corinna Jentzsch (Leiden University).
