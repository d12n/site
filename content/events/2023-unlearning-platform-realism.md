---
title: Unlearning Platform Realism
start: 2023-03-20 11:00:00+1
end: 2023-03-20 13:30:00+1
location: PdlC 1B01 (Living Lab)
---

# Unlearning Platform Realism

## A workshop with Miriyam Aouragh, Femke Snelting, and Marloes de Valk

This workshop was financially supported by [SCM](https://www.universiteitleiden.nl/en/social-citizenship-and-migration).
