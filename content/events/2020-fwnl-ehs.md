---
title: "Allergic to Wifi?"
start: 2020-04-07 12:30:00+2
end: 2020-04-07 15:00:00+2
location: online (Jitsi Meet)
---

# Allergic to Wifi?

## A research presentation by Fieldwork NL students

CADS students Kirsten Barink, Milène van der Geest, Claire van den Helder, and Pim Ruhe presented their research on electro-hypersensitivity (EHS).

Bert de Graaff (Erasmus University Rotterdam) responded.
