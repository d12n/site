---
title: Thinking through Drawing and Illustration
start: 2023-04-17 12:30:00+2
end: 2023-04-17 15:00:00+2
location: Centre for Digital Scholarship, University Library, Witte Singel 27, Leiden 
---

# Thinking through Drawing and Illustration

## A workshop with Ulrike Uhlig

The d12n Research Cluster invites you to join a workshop on how you can use drawing and illustration in your work as a researcher, educator, and public communicator. The workshop will be facilitated by Ulrike Uhlig.

[More information][ul_link]

[ul_link]: https://www.universiteitleiden.nl/en/events/2023/04/thinking-through-drawing-and-illustration-a-workshop-with-ulrike-uhlig
