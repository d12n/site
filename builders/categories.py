from pathlib import Path
from urllib.parse import urljoin, quote_from_bytes

import frontmatter
from icalendar import Calendar, Event
from slugify import slugify


def events(sub, site, template, **kwargs):
    outdir = Path(site.outpath)
    cat = sub.name
    catdir = outdir / cat
    entries = {
        e.stem: frontmatter.load(e)
        for e in sub.glob("*.md")
        if not e.name.startswith("_")
    }
    cal_events = []
    event_pages = []
    for name, entry in entries.items():
        slug = entry.get("slug") or slugify(entry.get("title", name))
        title = entry.get("title", name)
        canonical_entry = urljoin(site.env.globals["base_url"], f"{cat}/{slug}")
        event_pages.append({
            "slug": slug, "title": title, "start_time": entry.get("start")
        })
        cal_event = Event()
        cal_event.add("summary", title)
        cal_event.add("description", canonical_entry)
        cal_event.add("dtstart", entry.get("start"))
        cal_event.add("dtend", entry.get("end"))
        cal_event.add("location", entry.get("location"))
        cal_events.append(cal_event)
        rendered = template.stream(
            {
                "canonical": canonical_entry,
                "category": cat,
                "content": entry.content,
                "ics": quote_from_bytes(cal_event.to_ical()),
                "slug": slug,
                **entry.metadata,
            }
        )
        entrydir = catdir / slug
        entrydir.mkdir(parents=True, exist_ok=True)
        dest = entrydir / "index.html"
        with dest.open("wb") as f:
            rendered.dump(f, encoding="utf-8")
    cal = Calendar()
    for ce in cal_events:
        cal.add_component(ce)
    caldest = catdir / "events.ics"
    with caldest.open("wb") as f:
        f.write(cal.to_ical())
    catindex = catdir / "index.html"
    canonical_index = urljoin(site.env.globals["base_url"], cat)
    with catindex.open("wb") as f:
        template.stream({
            "index": True,
            "all_entries": event_pages,
            "canonical": canonical_index,
            "category": cat,
        }).dump(f, encoding="utf-8")
