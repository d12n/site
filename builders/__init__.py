from pathlib import Path
from urllib.parse import urljoin

import frontmatter
from slugify import slugify

from .categories import events


def default(sub, site, template, **kwargs):
    outdir = Path(site.outpath)
    cat = sub.name
    catdir = outdir / cat
    entries = {
        e.stem: frontmatter.load(e)
        for e in sub.glob("*.md")
        if not e.name.startswith("_")
    }
    for name, entry in entries.items():
        slug = entry.get("slug") or slugify(entry.get("title", name))
        canonical = urljoin(site.env.globals["base_url"], f"{cat}/{slug}")
        rendered = template.stream(
            {
                "all_entries": entries,
                "canonical": canonical,
                "category": cat,
                "content": entry.content,
                **entry.metadata,
            }
        )
        entrydir = catdir / slug
        entrydir.mkdir(parents=True, exist_ok=True)
        dest = entrydir / "index.html"
        with dest.open("wb") as f:
            rendered.dump(f, encoding="utf-8")
