with import <nixpkgs> {};

stdenvNoCC.mkDerivation {
  pname = "d12n-site";
  version = "0.2.3";

  src = ./.;

  buildInputs = with pkgs.python3.pkgs; [
    icalendar
    pypandoc
    python-frontmatter
    python-slugify
    pytz
    staticjinja
    tomli
  ];

  buildPhase = ''
    python build.py
  '';

  installPhase = ''
    mkdir -p $out
    cp -r static/* $out
    cp -r public/* $out
  '';

  meta = with lib; {
    description = "d12n-site";
    homepage = "https://d12n.leiden.edu";
  };
}
